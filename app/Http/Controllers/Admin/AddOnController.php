<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AddOn;
use DB;

class AddOnController extends Controller
{
    public function index()
    {
        $data = AddOn::all();
        return view('admin.addOn.index',\compact('data'));
    }

    public function create()
    {
        return view('admin.addOn.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_en'           => 'required',
            'name_ur'           => 'required',
        ]);
        try {
            if(isset($request->id))
            {
                $success_msg    = "Data is Successfully Updated.";
            }
            else{
                $success_msg    = "Data is Successfully Added.";
            }

            DB::transaction(function() use ($request,$success_msg){

                if(isset($request->id))
                {
                    $addOn                           = AddOn::findOrFail($request->id);
                    $addOn->translate('en')->name    = $request->name_en;
                    $addOn->translate('ur')->name    = $request->name_ur;
                    $addOn->active                   = $request->has('active')?1:0;
                    $addOn->save();
                }
                else{
                    $data = [
                        'en' => [
                            'name'      => $request->name_en,
                        ],
                        'ur' => [
                            'name'      => $request->name_ur,
                        ],
                        'active'        => $request->has('active')?1:0,
                    ];
                    $addOn = AddOn::create($data);
                }
            });

        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is missing!');
        }
        return \redirect()->route('admin.addOn')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data = AddOn::find($id);
        return view('admin.addOn.create',compact('data'));
    }

    public function destroy($id)
    {
        try {
            $addOn = AddOn::find($id);
            $addOn->delete();
        } 
        catch (\Throwable $th) 
        {
            return \redirect()->route('admin.addOn')->with('error',"Record was not deleted!");
        }
        return \redirect()->route('admin.addOn')->with('success',"Record was deleted successfully!");
    }
}
