<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use File;
use Image;
use DB;


class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::all();
        return view('admin.categories.index',compact('data'));
    }

    public function create()
    {
        $category_parent = Category::all();
        return view('admin.categories.create',compact('category_parent'));
    }

    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'name_en'           => 'required',
            'name_ur'           => 'required',
        ]);
        if(isset($request->id))
        {
            $success_msg    = "Data is Successfully Updated.";
        }
        else{
            $success_msg    = "Data is Successfully Added.";
        }
        try{
            
            DB::transaction(function() use ($request){
                if(isset($request->id))
                {
                    $category                           = Category::findOrFail($request->id);
                    $category->translate('en')->name    = $request->name_en;
                    $category->translate('ur')->name    = $request->name_ur;
                    $category->parent_id                = @$request->parent_id;

                    if($request->hasFile('image'))
                    {
                        File::delete(public_path('/backend/images/category'.$category->image));
                        $imageName = time().'.'.$request->image->getClientOriginalExtension();
                        $request->image->move(public_path('/backend/images/category'), $imageName);
                        $category->image = $imageName;
                    }
                    $category->save();
                }
                else{
                    if($request->hasFile('image'))
                    {
                        $imageName = time().'.'.$request->image->getClientOriginalExtension();
                    }
                    $data = [
                        'en' => [
                            'name'      => $request->name_en,
                        ],
                        'ur' => [
                            'name'      => $request->name_ur,
                        ],
                        'parent_id'     => @$request->parent_id,
                        'image'         => @$imageName,
                    ];
                    $category = Category::create($data);
                    Image::make($request->image->getRealPath())->resize(600,600,function($constraint){
                    })->save(public_path('/backend/images/category').'/'.$imageName);
                }
            });
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error',$th);
        }

        return \redirect()->route('admin.category')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data = Category::find($id);
        $category_parent = Category::all();
        return view('admin.categories.create',compact('data','category_parent'));
    }
}
