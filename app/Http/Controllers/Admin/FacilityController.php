<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Facility;
use App\Models\Category;
use DB;


class FacilityController extends Controller
{
    public function index()
    {
        $data = Facility::all();
        return view('admin.facilities.index',\compact('data'));
    }

    public function create()
    {
        $category = Category::all();
        return view('admin.facilities.create',compact('category'));
    }

    public function store(Request $request)
    {
        // dd($request);

        // $temp = [
        //     'Electricity', 'Furnished', 'Generator', 'Parking', 'Air conditioning', 'Kitchen', 'Microwave', 'Swimming pool', 'Sofas', 'Dining table', 'Chairs', 'Beds', 'Outdoor furniture', 'Water', 'Gas'
        // ];

        // foreach ($temp as $key => $value) {
        //     $Facility           = new Facility;
        //     $Facility->name         = $value;
        //     $Facility->category_id  = 1;
        //     $Facility->active       = 1;
        //     $Facility->save();
        // }

        $request->validate([
            'name_en'           => 'required',
            'name_ur'           => 'required',
            'category_id'       => 'required',
        ]);
        try {
            $success_msg = "" ;

            DB::transaction(function() use ($request,$success_msg){

                if(isset($request->id))
                {
                    $success_msg                        = "Data is Successfully Updated.";
                    $Facility                           = Facility::findOrFail($request->id);
                    $Facility->translate('en')->name    = $request->name_en;
                    $Facility->translate('ur')->name    = $request->name_ur;
                    $Facility->category_id              = $request->category_id;
                    $Facility->active                   = $request->has('active')?1:0;
                    $Facility->save();
                }
                else{
                    $success_msg        = "Data is Successfully Added.";
                    $data = [
                        'en' => [
                            'name'      => $request->name_en,
                        ],
                        'ur' => [
                            'name'      => $request->name_ur,
                        ],
                        'category_id'   => $request->category_id,
                        'active'        => $request->has('active')?1:0,
                    ];
                    $Facility = Facility::create($data);
                }
            });

        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is missing!');
        }
        return \redirect()->route('admin.facility')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data = Facility::find($id);
        $category = Category::all();
        return view('admin.facilities.create',compact('data','category'));
    }

    public function destroy($id)
    {
        try {
            $facility = Facility::find($id);
            $facility->delete();
        } 
        catch (\Throwable $th) 
        {
            return \redirect()->route('admin.facility')->with('error',"Record was not deleted!");
        }
        return \redirect()->route('admin.facility')->with('success',"Record was deleted successfully!");
    }
}
