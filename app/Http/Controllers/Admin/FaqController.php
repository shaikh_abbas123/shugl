<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq; 
use DB;

class FaqController extends Controller
{
    public function index()
    {
        $data = Faq::all();
        return view('admin.faqs.index',\compact('data'));
    }

    public function create()
    {
        return view('admin.faqs.create');
    }

    public function store(Request $request)
    {
        // dd($request);

    //     "question_en" => "What is a FAQ page?"
    //   "question_ur" => "سوالات کا صفحہ کیا ہے؟"
    //   "answer_en" => "FAQ stands for “Frequently Asked Questions.” An FAQ is a list of commonly asked questions and answers on a website about topics such as hours, shipping and handling, product information, and return policies. ◀"
    //   "answer_ur" => "عمومی سوالات کا مطلب ہے "اکثر پوچھے گئے سوالات" ایک عمومی سوالات ویب سائٹ پر عام طور پر پوچھے جانے والے سوالات اور جوابات کی فہرست ہوتی ہے جیسے کہ گھنٹے ، شپنگ اور ہینڈلنگ ، مصنوعات کی معلومات اور واپسی کی پالیسیاں۔ ◀"

        $request->validate([
            "question_en"   => 'required',
            "question_ur"   => 'required',
            "answer_en"     => 'required',
            "answer_ur"     => 'required',
        ]);
        try {

            if(isset($request->id))
            {
                $success_msg    = "Data is Successfully Updated.";
            }
            else{
                $success_msg    = "Data is Successfully Added.";
            }

            DB::transaction(function() use ($request){

                if(isset($request->id))
                {
                    $faq                                = Faq::findOrFail($request->id);
                    $faq->translate('en')->question     = $request->question_en;
                    $faq->translate('en')->answer       = $request->answer_en;
                    $faq->translate('ur')->question     = $request->question_ur;
                    $faq->translate('ur')->answer       = $request->answer_ur;
                    $faq->active                        = $request->has('active')?1:0;
                    $faq->save();
                }
                else{
                    $data = [
                        'en' => [
                            'question'  => $request->question_en,
                            'answer'    => $request->answer_en,
                        ],
                        'ur' => [
                            'question'  => $request->question_ur,
                            'answer'    => $request->answer_ur,
                        ],
                        'active'        => $request->has('active')?1:0,
                    ];
                    $faq = Faq::create($data);
                }
            });

        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is missing!');
        }
        return \redirect()->route('admin.faq')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data = Faq::find($id);
        // dd($data);
        return view('admin.faqs.create',compact('data'));
    }

}
