<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Facility;
use App\Category;
use Str;
use File;
use Image;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
        $data       = Product::all();
        return view('admin.products.index',\compact('data'));
    }

    public function create()
    {
        $category = Category::all();
        $facility = Facility::all();
        return view('admin.products.create',compact('category','facility'));
    }

    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'name'               => 'required',
            'price'              => 'required',
            'category_id'           => 'required',
            // 'discount_percent'   => 'required',
            'description'        => 'required',
        ]);

        try {
            $success_msg    = "Data is Successfully Added.";

            $product = new Product;

            $product->name              = $request->name;
            $product->price             = $request->price;
            $product->category_id       = $request->category_id;
            $product->discount_percent  = ($request->discount_percent != null || isset($request->discount_percent))? $request->discount_percent:0;
            $product->description       = $request->description;
            $product->save();

            if($request->hasFile('product_image'))
            {
                foreach($request->product_image as $key =>$item)
                {
                    $imageName = Str::random(5).'_'.time().'.'.$item->extension();
                    Image::make($item->getRealPath())->resize(600,600,function($constraint){
                    })->save(public_path('/backend/images/products').'/'.$imageName);
                    ProductImage::create(['product_id' => $product->id,'image'=>$imageName]);
                }
            }
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is missing!');
        }
        return \redirect()->route('admin.product')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data       = Product::find($id);
        $category = Category::all();
        return view('admin.products.create',compact('data','category'));
    }

    public function destroy($id)
    {
        try {
            $Product = Product::find($id);
            $Product->delete();
        } 
        catch (\Throwable $th) 
        {
            return \redirect()->route('admin.product')->with('error',"Record was not deleted!");
        }
        return \redirect()->route('admin.product')->with('success',"Record was deleted successfully!");
    }
}
