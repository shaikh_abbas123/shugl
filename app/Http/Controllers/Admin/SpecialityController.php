<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Speciality;
use App\Models\Category;
use DB;


class SpecialityController extends Controller
{
    public function index()
    {
        $data = Speciality::all();
        return view('admin.specialities.index',\compact('data'));
    }

    public function create()
    {
        $category = Category::all();
        return view('admin.specialities.create',compact('category'));
    }

    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'name_en'           => 'required',
            'name_ur'           => 'required',
            'category_id'       => 'required',
        ]);
        try {

            $success_msg = "" ;

            DB::transaction(function() use ($request,$success_msg){

                if(isset($request->id))
                {
                    $success_msg                        = "Data is Successfully Updated.";
                    $Speciality                           = Speciality::findOrFail($request->id);
                    $Speciality->translate('en')->name    = $request->name_en;
                    $Speciality->translate('ur')->name    = $request->name_ur;
                    $Speciality->category_id              = $request->category_id;
                    $Speciality->active                   = $request->has('active')?1:0;
                    $Speciality->save();
                }
                else{
                    $success_msg        = "Data is Successfully Added.";
                    $data = [
                        'en' => [
                            'name'      => $request->name_en,
                        ],
                        'ur' => [
                            'name'      => $request->name_ur,
                        ],
                        'category_id'   => $request->category_id,
                        'active'        => $request->has('active')?1:0,
                    ];
                    $Speciality = Speciality::create($data);
                }
            });

        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is missing!');
        }
        return \redirect()->route('admin.speciality')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data = Speciality::find($id);
        $category = Category::all();
        return view('admin.specialities.create',compact('data','category'));
    }

    public function destroy($id)
    {
        try {
            $Speciality = Speciality::find($id);
            $Speciality->delete();
        } 
        catch (\Throwable $th) 
        {
            return \redirect()->route('admin.speciality')->with('error',"Record was not deleted!");
        }
        return \redirect()->route('admin.speciality')->with('success',"Record was deleted successfully!");
    }
}
