<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vendor;
use Hash;

class VendorController extends Controller
{
    public function index()
    {
        $data = Vendor::all();
        return view('admin.vendors.index',\compact('data'));
    }

    public function create()
    {
        return view('admin.vendors.create');

    }

    public function store(Request $request)
    {
        // dd($request);

        if(isset($request->id))
        {
            if($request->password != null)
            {
                $request->validate([
                    'name'              => 'required',
                    'email'             => 'required|email:rfc,dns',
                    'password'          => 'required|min:8',
                    'confirm_password'  => 'required_with:password|same:password|min:8',
                ]);
            }
            else
            {
                $request->validate([
                    'name'              => 'required',
                    'email'             => 'required|email:rfc,dns',
                ]);
            }
            
            $success_msg    = "Data is Successfully Updated.";
            $vendor         = Vendor::find($request->id);
        }
        else{

            $request->validate([
                'name'              => 'required',
                'email'             => 'required|email:rfc,dns|unique:vendors',
                'password'          => 'required|min:8',
                'confirm_password'  => 'required_with:password|same:password|min:8',
            ]);
            
            $success_msg    = "Data is Successfully Added.";
            $vendor         = new Vendor;
            
        }

        try {
            $vendor->name       = $request->name;
            $vendor->email      = $request->email;
            if(isset($request->id))
            {
                if($request->password != null)
                {
                    $vendor->password   = Hash::make($request->password);
                }
            }
            else{
                $vendor->password   = Hash::make($request->password);
            }
            $vendor->active     = $request->has('active')?1:0;
            $vendor->save();
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        return \redirect()->route('admin.vendor')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data = Vendor::find($id);
        return view('admin.vendors.create',compact('data'));
    }

    public function destroy($id)
    {
        try {
            $vendor = Vendor::find( $id );
            $vendor->delete();
        } 
        catch (\Throwable $th) 
        {
            return \redirect()->route('admin.vendor')->with('error',"Record was not deleted!");
        }
        return \redirect()->route('admin.vendor')->with('success',"Record was deleted successfully!");
    }
}
