<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $category = Category::orderBy('sort', 'asc')->get();
        // dd($category);
        return view('website.index')->with('category', $category);
    }
    public function listing()
    {
        return view('website.listing');
    }
    public function details()
    {
        return view('website.details');
    }
    public function bookingdate()
    {
        return view('website.bookingdate');
    }
    public function checkout()
    {
        return view('website.checkout');
    }
    public function beachhutdetails()
    {
        return view('website.forms.beachhutdetails');
    }
    public function farmhousedetails()
    {
        return view('website.forms.farmhousedetails');
    }
    public function banquetdetails()
    {
        return view('website.forms.banquetdetails');
    }
    public function hoteldetails()
    {
        return view('website.forms.hoteldetails');
    }
    public function transporterdetails()
    {
        return view('website.forms.transporterdetails');
    }
    public function cateringdetails()
    {
        return view('website.forms.cateringdetails');
    }
    public function photographer()
    {
        return view('website.forms.photographer');
    }
    public function vendordashboard()
    {
        return view('website.vendor.dashboard');
    }
    public function bookingsuccess()
    {
        return view('website.bookingsuccess');
    }
   
}

