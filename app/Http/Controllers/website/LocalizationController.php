<?php
namespace App\Http\Controllers\website;
use App\Http\Controllers\Controller;
use App;
use Illuminate\Http\Request;
class LocalizationController extends Controller {
	public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index($lang){
        App::setlocale($lang);
        // app()->setLocale($lang);
        session()->put('lang', $lang);
        // dd(app()->getLocale());
        return redirect()->back();
    }
}