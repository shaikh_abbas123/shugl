<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class AddOn extends Model
{
    use SoftDeletes;
    use Translatable;

    protected $table = "add_on";
    public $translatedAttributes = ['name'];

    protected $fillable = [
        'active',
    ];
}
