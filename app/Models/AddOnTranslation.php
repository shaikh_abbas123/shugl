<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddOnTranslation extends Model
{
    protected $table = "add_on_translations";
    protected $fillable = ['name'];
    public $timestamps = false;
}
