<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Category extends Model
{
    use SoftDeletes;
    use Translatable;
    
    protected $table = "categories";
    public $translatedAttributes = ['name'];

    protected $fillable = [
        'image','parent_id',
    ];
    
    public function parent()
    {
        return $this->belongsTo('App\Models\Category','parent_id', 'id');
    }

    public function category_text(){
        return $this->hasOne('App\Models\CategoryTranslation', 'category_id', 'id');
    }
}
