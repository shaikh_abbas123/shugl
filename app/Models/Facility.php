<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Facility extends Model
{
    use SoftDeletes;
    use Translatable;

    protected $table = "facilities";
    public $translatedAttributes = ['name'];

    protected $fillable = [
        'category_id', 'active',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category','category_id', 'id');
    }
}
