<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacilityTranslation extends Model
{
    protected $table = "facility_translations";
    protected $fillable = ['name'];
    public $timestamps = false;
}
