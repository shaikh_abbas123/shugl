<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialityTranslation extends Model
{
    protected $table = "speciality_translations";
    protected $fillable = ['name'];
    public $timestamps = false;
}
