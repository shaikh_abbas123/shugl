<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialityTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speciality_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();
            $table->text('name')->nullable();
            $table->foreignId('speciality_id')->constrained();
            $table->unique(['speciality_id', 'locale']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speciality_translations');
    }
}
