<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddOnTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_on_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            // $table->foreignId('add_on_id')->constrained();
            $table->foreignId('add_on_id')->nullable()->references('id')->on('add_on');
            $table->unique(['add_on_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_on_translations');
    }
}
