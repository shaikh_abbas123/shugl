<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('12345678'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'name' => 'Customer',
            'email' => 'customer@customer.com',
            'password' => Hash::make('12345678'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('vendors')->insert([
            'name' => 'Vendor',
            'email' => 'vendor@vendor.com',
            'password' => Hash::make('12345678'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        $mul_rows= [
            ['image' => '1628241713.png', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'sort' => 1 ],
            ['image' => '1628241723.png', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'sort' => 2 ],
            ['image' => '1628241753.png', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'sort' => 3 ],
            ['image' => '1628241819.png', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'sort' => 4 ],
            ['image' => '1628241788.png', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'sort' => 5 ],
            ['image' => '1628241796.png', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'sort' => 6 ],
            ['image' => '1628241833.png', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'sort' => 7 ],
            ['image' => '1628241763.png', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),'sort' => 8 ],
        ];
        foreach ($mul_rows as $rows) 
        {
            DB::table('categories')->insert($rows);
        }

         $mul_rows2= [
            [ 'name' => 'Beach Huts','locale'  => 'en', 'category_id' => 1],
            [ 'name' => 'بیچ ہٹس۔','locale'   => 'ur', 'category_id' => 1],
            [ 'name' => 'Farmhouses','locale' => 'en', 'category_id' => 2 ],
            [ 'name' => 'فارم ہاؤسز۔', 'locale' => 'ur', 'category_id' => 2 ],
            [ 'name' => 'Lawns/Banquets','locale' => 'en', 'category_id' => 3 ],
            [ 'name' => 'لان/ضیافتیں','locale' => 'ur', 'category_id' => 3 ],
            [ 'name' => 'Hotels','locale' => 'en', 'category_id' => 4 ],
            [ 'name' => 'ہوٹل','locale' => 'ur', 'category_id' => 4 ],
            [ 'name' => 'Event Decorators','locale' => 'en', 'category_id' => 5 ],
            [ 'name' => 'ایونٹ ڈیکوریٹرز۔','locale' => 'ur', 'category_id' => 5 ],
            [ 'name' => 'Transporters','locale' => 'en', 'category_id' => 6 ],
            [ 'name' => 'ٹرانسپورٹرز','locale' => 'ur', 'category_id' => 6 ],
            [ 'name' => 'Caterers','locale' => 'en', 'category_id' => 7 ],
            [ 'name' => 'کیٹررز','locale' => 'ur', 'category_id' => 7 ],
            [ 'name' => 'Photographers','locale' => 'en', 'category_id' => 8 ],
            [ 'name' => 'فوٹوگرافرز۔','locale' => 'ur', 'category_id' => 8 ],
            
        ];
        foreach ($mul_rows2 as $rows) 
        {
            DB::table('category_translations')->insert($rows);
        }
        
    }
}
