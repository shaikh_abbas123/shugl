@extends('admin.layouts.app')
@push('custom-css')

@endpush
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Add On</h3>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <a href="{{route('admin.addOnCreate')}}" class="btn btn-primary mb-3">Insert Add On</a>
                            <table class="table table-bordered table-striped" id="data-table">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Name(English)</th>
                                        <th>Name(اردو)</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 0;
                                    @endphp
                                    @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{++$i}}</td>
                                        <td>{{@$item->translate('en')->name}}</td>
                                        <td>{{@$item->translate('ur')->name}}</td>
                                        <td>{{(@$item->active == 1)? "Active":"Deactive"}}</td>
                                        <td>
                                            <form action="{{ route('admin.addOnDelete',@$item->id)}}" method="get">
                                                <a href="{{route('admin.addOnEdit',@$item->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                <button type="submit" class="btn btn-danger btn-delete"><i class="fas fa-trash-alt"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let form = _this.closest('form');
       
        Swal.fire({
            title: 'Do you want to save the changes?',
            showCancelButton: true,
            confirmButtonText: `Save`,
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
            else{
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })
</script>
@endpush