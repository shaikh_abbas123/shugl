@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />
<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Category</h3>
                        </div>

                        <div class="" style="display: none" id="msg_alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> <span id="ajax_msg_title">Success!</span> </h5>
                            <p id="ajax_msg"></p>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <div class="card-body">
                            {{-- <a href="" class="btn btn-primary float-right">
                                Add Category
                            </a> --}}
                            <form action="{{route('admin.categoryStore')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    {{-- <div class="form-group col-md-6">
                                        <label for="">Category Name<span style="color: red">*</span></label>
                                        <input type="text" name="name"
                                            class="form-control  @error('name') is-invalid @enderror"
                                            value="{{(old('name')!=null)? (old('name')):(isset($data->name)? $data->name:'')}}">
                                        @error('name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div> --}}

                                    @if (isset($data))
                                    <div class="form-group col-md-6">
                                        <label for="name_en">Name(English)</label>
                                        <input type="text" name="name_en" class="form-control"
                                            value="{{(old('name_en')!=null)? (old('name_en')):(isset($data->translate('en')->name)? @$data->translate('en')->name:'')}}" id="name_en">
                                        @error('name_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="name_ur">Name(اردو)</label>
                                        <input type="text" name="name_ur" class="form-control"
                                            value="{{(old('name_ur')!=null)? (old('name_ur')):(isset($data->translate('ur')->name)? @$data->translate('ur')->name:'')}}" id="name_ur">
                                        @error('name_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                @else
                                    <div class="form-group col-md-6">
                                        <label for="name_en">Name(English)</label>
                                        <input type="text" name="name_en" class="form-control"
                                            value="{{(old('name_en')!=null)? old('name_en'):''}}" id="name_en">
                                        @error('name_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="name_ur">Name(اردو)</label>
                                        <input type="text" name="name_ur" class="form-control"
                                            value="{{(old('name_ur')!=null)? old('name_ur'):''}}" id="name_ur">
                                        @error('name_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                @endif

                                    <div class="form-group col-md-6">
                                        <label for="">Select Parent Category<small>(Optional)</small></label>
                                        <select name="parent_id" id="" class="form-control  @error('parent_id') is-invalid @enderror">
                                            <option value="0" disabled selected>Select Option</option>
                                            @foreach ($category_parent as $item)
                                                <option value="{{$item->id}}" {{ (isset($data->parent_id)? (($data->parent_id == $item->id)? "selected":"" ):"" ) }} >{{$item->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="">Image</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" accept="image/*" id="image" name="image" class=" @error('image') is-invalid @enderror">
                                                {{-- <label class="custom-file-label" for="image">Choose file</label> --}}
                                            </div>
                                        </div>
                                        @error('image')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    

                                    @if (isset($data->image))
                                    <div class="form-group col-md-6">
                                        <img src="{{ @asset('backend/images/category')."/".@$data->image}}" height="150" width="auto" alt="" srcset="">
                                    </div>
                                    @endif

                                    
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('admin.category')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>



@endsection


@push('custom-script')
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"></script>

<script>
    $(function () {
        $("#data-table").DataTable();
    });

 

    
   

</script>
@endpush
