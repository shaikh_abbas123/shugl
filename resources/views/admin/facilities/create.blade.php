@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Facility</h3>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickForm" action="{{route('admin.facilityStore')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @if (isset($data->id))
                                <input type="hidden" name="id" value="{{ @$data->id }}">
                            @endif
                            <div class="card-body row">
                                @if (isset($data))
                                    <div class="form-group col-md-6">
                                        <label for="name_en">Name(English)</label>
                                        <input type="text" name="name_en" class="form-control"
                                            value="{{(old('name_en')!=null)? (old('name_en')):(isset($data->translate('en')->name)? @$data->translate('en')->name:'')}}" id="name_en">
                                        @error('name_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="name_ur">Name(اردو)</label>
                                        <input type="text" name="name_ur" class="form-control"
                                            value="{{(old('name_ur')!=null)? (old('name_ur')):(isset($data->translate('ur')->name)? @$data->translate('ur')->name:'')}}" id="name_ur">
                                        @error('name_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                @else
                                    <div class="form-group col-md-6">
                                        <label for="name_en">Name(English)</label>
                                        <input type="text" name="name_en" class="form-control"
                                            value="{{(old('name_en')!=null)? old('name_en'):''}}" id="name_en">
                                        @error('name_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="name_ur">Name(اردو)</label>
                                        <input type="text" name="name_ur" class="form-control"
                                            value="{{(old('name_ur')!=null)? old('name_ur'):''}}" id="name_ur">
                                        @error('name_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                @endif
                                

                                <div class="form-group col-md-6">
                                    <label for="">Select Category</label>
                                    <select name="category_id" id="" class="form-control  @error('category_id') is-invalid @enderror">
                                        <option value="0" disabled selected>Select Option</option>
                                        @if (old('category_id'))
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}" {{ ((old('category_id') != null)? ((old('category_id') == $item->id)? "selected":"" ):"" ) }} >{{$item->name}}</option>
                                            @endforeach
                                        @else
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}" {{ (isset($data->category_id)? (($data->category_id == $item->id)? "selected":"" ):"" ) }} >{{$item->name}}</option>
                                            @endforeach
                                        @endif
                                        
                                    </select>
                                    @error('category_id')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <div class="form-check ">
                                    <input type="checkbox" class="form-check-input" name="active" id="active"  {{old('active')=="on"||@$data->active==1? 'checked':''}}>  Is Active?
                                    @error('active')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror 
                                    </div>
                                </div>


                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Save</button>
                                <a href="{{route('admin.facility')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@push('custom-script')


<script> 
$('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showCancelButton: true,
            confirmButtonText: `Save`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                $('#quickForm').submit();
            } else{
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })
</script>
@endpush