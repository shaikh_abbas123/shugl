@extends('admin.layouts.app')
@push('custom-css')
<style>
    /* .card-primary:not(.card-outline)>.card-header {
        background-color: #dc3545;
    } */
</style>
@endpush

@section('content')
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            {{Session::get('error')}}
                    </div>
                @endif
                

                



                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-question">Add FAQ</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('admin.faqStore')}}" enctype="multipart/form-data">
                            @csrf
                            @if (isset($data->id))
                                <input type="hidden" name="id" value="{{ @$data->id }}">
                                <div class="card-body row">
                                    <div class="form-group col-md-6">
                                        <label>Question(English)</label>
                                        <input type="text" name="question_en"
                                            class="form-control @error('question_en') is-invalid @enderror"
                                            placeholder="Enter question" value="{{ @$data->translate('en')->question }}">
                                        @error('question_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
    
                                    <div class="form-group col-md-6">
                                        <label>Question(اردو)</label>
                                        <input type="text" name="question_ur"
                                            class="form-control @error('question_ur') is-invalid @enderror"
                                            placeholder="Enter question" value="{{ @$data->translate('ur')->question }}">
                                        @error('question_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label for="">Answer(English)</label>
                                        <textarea class="form-control @error('answer_en') is-invalid @enderror"
                                            name="answer_en" rows="3" placeholder="Enter ...">{{ @$data->translate('en')->answer }}</textarea>
                                        @error('answer_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
    
                                    <div class="form-group col-md-6">
                                        <label for="">Answer(اردو)</label>
                                        <textarea class="form-control @error('answer_ur') is-invalid @enderror"
                                            name="answer_ur" rows="3" placeholder="Enter ...">{{ @$data->translate('ur')->answer }}</textarea>
                                        @error('answer_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                            @else
                                <div class="card-body row">
                                    <div class="form-group col-md-6">
                                        <label>Question(English)</label>
                                        <input type="text" name="question_en"
                                            class="form-control @error('question_en') is-invalid @enderror"
                                            placeholder="Enter question" value="">
                                        @error('question_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Question(اردو)</label>
                                        <input type="text" name="question_ur"
                                            class="form-control @error('question_ur') is-invalid @enderror"
                                            placeholder="Enter question" value="">
                                        @error('question_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label for="">Answer(English)</label>
                                        <textarea class="form-control @error('answer_en') is-invalid @enderror"
                                            name="answer_en" rows="3" placeholder="Enter ..."></textarea>
                                        @error('answer_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="">Answer(اردو)</label>
                                        <textarea class="form-control @error('answer_ur') is-invalid @enderror"
                                            name="answer_ur" rows="3" placeholder="Enter ..."></textarea>
                                        @error('answer_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                            @endif

                            <div class="form-group col-md-12">
                                <div class="form-check ">
                                <input type="checkbox" class="form-check-input" name="active" id="active"  {{old('active')=="on"||@$data->active==1? 'checked':''}}>  Is Active?
                                @error('active')
                                <p class="text-danger text-sm">{{$message}}</p>
                                @enderror 
                                </div>
                            </div>
                            
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.faq')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection