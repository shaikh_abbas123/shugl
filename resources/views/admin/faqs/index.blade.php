@extends('admin.layouts.app')
@push('custom-css')
<!-- DataTables -->
{{-- <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}"> --}}
<style>
    /* .card-primary:not(.card-outline)>.card-header {
        background-color: #dc3545;
    } */
</style>
@endpush

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::has('error'))
                <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    {{Session::get('error')}}
                </div>
                @endif

                <div class=" col-md-12 mt-2">
                    <div class="card-header">
                        <h3 class="card-title">FAQ</h3>
                        <a href="{{route('admin.faqCreate')}}" style="float: right" class="btn btn-primary">Add FAQ</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-12" id="accordion">
                            @foreach ($data as $item)
                                <div class="card card-danger card-outline">
                                    <a class="d-block w-100" data-toggle="collapse" href="#{{'collapse'.@$item->id}}">
                                        <div class="card-header w-100">
                                            <h4 class="card-title w-100">
                                                {{@$item->translate('en')->question}}
                                            </h4>
                                            <h4 class="card-title float-right">
                                                {{@$item->translate('ur')->question}}
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="{{'collapse'.@$item->id}}" class="collapse" data-parent="#accordion">
                                        <div class="card-body w-100">
                                            {{@$item->translate('en')->answer}}
                                        </div>
                                        <div class="card-body float-right">
                                            {{@$item->translate('ur')->answer}}
                                        </div>
                                        <br>
                                        <div class="m-2" style="">

                                            <form action="{{route('admin.faqDelete',$item->id)}}" method="GET" class="delete-form">
                                                
                                                <a href="{{ route('admin.faqEdit',$item->id) }}" class="btn btn-primary">Edit</a>
                
                                                <input type="hidden" name="id" value="{{$item->id}}">
                                                <button type="submit" class="btn btn-danger delete-btn">Delete</button>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@push('custom-script')

{{-- <script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('backend/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('backend/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script> --}}
<script>
    // $(function () {
    //     $("#example1").DataTable({
    //         "responsive": true,
    //         "lengthChange": false,
    //         "autoWidth": false,
    //         // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    //     $('#example2').DataTable({
    //         "paging": true,
    //         "lengthChange": false,
    //         "searching": false,
    //         "ordering": true,
    //         "info": true,
    //         "autoWidth": false,
    //         "responsive": true,
    //     });
    // });

    // $('.delete-btn').click(function (e) {
    //     e.preventDefault();
    //     let _this = $(this);
    //     swal({
    //             title: "Are you sure?",
    //             text: "Once deleted, you will not be able to recover this record!",
    //             icon: "warning",
    //             buttons: true,
    //             dangerMode: true,
    //         })
    //         .then((willDelete) => {
    //             if (willDelete) {
    //                 swal("Poof! Your record has been deleted!", {
    //                     icon: "success",
    //                 });
    //                 $(_this).closest('.delete-form').submit();
    //             } else {
    //                 swal("Your record is safe!");
    //             }
    //         });
    // });
</script>
@endpush