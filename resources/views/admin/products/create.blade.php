@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{ asset('backend/myplugin/fileuploader/css/jquery.fileuploader.css') }}">
<link rel="stylesheet" href="{{ asset('backend/myplugin/fileuploader/css/jquery.fileuploader-theme-dragdrop.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css" integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg==" crossorigin="anonymous" />

@endpush

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Product</h3>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickForm" action="{{route('admin.productStore')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @if (isset($data->id))
                                <input type="hidden" name="id" value="{{ @$data->id }}">
                            @endif
                            <div class="card-body row">
                                <div class="form-group col-md-6">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control"
                                        value="{{(old('name')!=null)? (old('name')):(isset($data->name)? $data->name:'')}}" id="name">
                                    @error('name')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Select Category</label>
                                    <select name="category_id" id="" class="form-control  @error('category_id') is-invalid @enderror">
                                        <option value="0" disabled selected>Select Option</option>
                                        @if (old('category_id'))
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}" {{ ((old('category_id') != null)? ((old('category_id') == $item->id)? "selected":"" ):"" ) }} >{{$item->name}}</option>
                                            @endforeach
                                        @else
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}" {{ (isset($data->category_id)? (($data->category_id == $item->id)? "selected":"" ):"" ) }} >{{$item->name}}</option>
                                            @endforeach
                                        @endif
                                        
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="price">Price</label>
                                    <input type="number" name="price" class="form-control"
                                        value="{{(old('price')!=null)? (old('price')):(isset($data->price)? $data->price:'')}}" id="price">
                                    @error('price')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="discount_percent">Discount Percentage</label>
                                    <input type="number" name="discount_percent" class="form-control"
                                        value="{{(old('discount_percent')!=null)? (old('discount_percent')):(isset($data->discount_percent)? $data->discount_percent:'')}}" id="discount_percent">
                                    @error('discount_percent')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Description</span></label>
                                        <textarea  class="form-control  @error('description') is-invalid @enderror" name="description" id="" cols="30" rows="5">{{(old('description')!=null)? (old('description')):(isset($data->description)? $data->description:'')}}</textarea>
                                    @error('description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="row ml-2">
                                    <div class="form-group col-md-12 pb-2" >
                                        <label for="">Service Hour</label>
                                        <br>
                                        <div class="form-check d-inline-block pr-3 pl-3">
                                            <input type="checkbox" class="form-check-input" name="24_hours" id="24_hours"  {{old('24_hours')=="on"? 'checked':''}}>  24 Hours
    
                                        </div>
                                        <div class="form-check d-inline-block pr-3 pl-3">
                                            <input type="checkbox" class="form-check-input" name="12_hours" id="12_hours"  {{old('12_hours')=="on"? 'checked':''}}>  12 Hours
                                            
                                        </div>
                                        
                                        
                                    </div>

                                    <div class="form-group col-md-12 pb-2" >
                                        
                                        <div class="form-check d-inline-block pr-3 pl-3">
                                            <input type="checkbox" class="form-check-input " name="specific" id="specific"  {{old('specific')=="on"? 'checked':''}}> Specific 
                                            
                                        </div>
                                        <div class="form-check d-inline-block pr-3 pl-3">
                                            <small>Start Time</small>
                                            <input type="time" name="specific_time1" id="specific_time1" class="form-control" disabled> 
                                        </div>
                                        <div class="form-check d-inline-block pr-3 pl-3">
                                            <small>End Time</small>
                                            <input type="time" name="specific_time2" id="specific_time2" class="form-control" disabled> 
                                        </div>
                                        
                                    </div>
                                </div>
                                {{-- <div class="form-group col-md-12 pb-2" style="border-bottom: 1px solid #b9b6b6;">
                                    <label for="">Service Hour</label>
                                    <br>
                                    <div class="form-check d-inline-block pr-3 pl-3">
                                        <input type="checkbox" class="form-check-input" name="24_hours" id="24_hours"  {{old('24_hours')=="on"? 'checked':''}}>  24 Hours

                                    </div>
                                    <div class="form-check d-inline-block pr-3 pl-3">
                                        <input type="checkbox" class="form-check-input" name="12_hours" id="12_hours"  {{old('12_hours')=="on"? 'checked':''}}>  12 Hours
                                        
                                    </div>
                                    <div class="form-check d-inline-block pr-3 pl-3">
                                        <input type="checkbox" class="form-check-input " name="specific" id="specific"  {{old('specific')=="on"? 'checked':''}}> Specific 
                                        
                                    </div>
                                    <div class="form-check d-inline-block pr-3 pl-3">
                                        <small>Start Time</small>
                                        <input type="time" name="specific_time1" id="specific_time1" class="form-control" disabled> 
                                    </div>
                                    <div class="form-check d-inline-block pr-3 pl-3">
                                        <small>End Time</small>
                                        <input type="time" name="specific_time2" id="specific_time2" class="form-control" disabled> 
                                    </div>
                                    
                                </div> --}}

                                <div class="form-group col-md-4">
                                    <label for="">Area</label>
                                    <input type="text" class="form-control @error('area') is-invalid @enderror" name="area" id="">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="">Number of Floors</label>
                                    <input type="text" class="form-control @error('no_of_floors') is-invalid @enderror" name="no_of_floors" id="">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="">Number of Rooms</label>
                                    <input type="text" class="form-control @error('no_of_rooms') is-invalid @enderror" name="no_of_rooms" id="">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="product_image">Product Images</label>
                                    <input type="file" accept="image/*" multiple class="form-control-file @error('product_image') is-invalid @enderror" name="product_image[]" id="fileUploader">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Facilities</label>
                                    {{-- <input type="text" class="form-control @error('no_of_rooms') is-invalid @enderror" name="no_of_rooms" id=""> --}}
                                </div>


                                <div class="form-group col-md-12">
                                    <div class="form-check ">
                                    <input type="checkbox" class="form-check-input" name="active" id="active"  {{old('active')=="on"||@$data->active==1? 'checked':''}}>  Is Active?
                                    @error('active')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror 
                                    </div>
                                </div>

                                


                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Save</button>
                                <a href="{{route('admin.addOn')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@push('custom-script')
<script src="{{ asset('backend/myplugin/fileuploader/js/jquery.fileuploader.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js" integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g==" crossorigin="anonymous"></script>
    <script>
        $('#fileUploader').fileuploader({
            changeInput: '<div class="fileuploader-input p-3">' +
                '<div class="fileuploader-input-inner">' +
                '<img src="{{ asset("backend/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png") }}">' +
                '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                '<p>or</p>' +
                '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                '</div>' +
                '</div>',
            theme: 'dragdrop',
            limit: 6,
            addMore: false,
            extensions: ['jpg', 'jpeg', 'png'],
            onRemove: function(item) {
                $.post('', {
                    file: item.name,
                    data: {
                        image_file_id: "{{ @$data->id }}",
                        file: item.name,
                        image_post_file_id: item.data.image_file_id,
                        "_token": $('meta[name="csrf-token"]').attr('content')
                    }
                });
            },
            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            },
        });
        var fileInput = document.querySelector("#thumbnail_image")
        var ImageElement = document.querySelector("#productthumbnail")
        
        fileInput.addEventListener("change",function(){
            if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                ImageElement.setAttribute("src", e.target.result);
            }
            reader.readAsDataURL(fileInput.files[0]); // convert to base64 string
            }
        })
        
    </script>
    @if ($errors->any())
    <script>
        errors = '<?php echo json_encode($errors->all()); ?>';
        errors = errors.map((item)=>{
            return `<p class="m-0 text-danger">${item}</p>`
        })
        Swal.fire({title:"Validation Error",html: errors.join(' ')})
    </script>
    @endif

<script> 
$('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showCancelButton: true,
            confirmButtonText: `Save`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                $('#quickForm').submit();
            } else{
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });

    $("#specific").change(function (e) { 
        e.preventDefault();
        // console.log(this.checked);

        if(this.checked)
        {
            $('#specific_time1').prop('disabled',false);
            $('#specific_time2').prop('disabled',false);
        }
        else{
            $('#specific_time1').prop('disabled',true);
            $('#specific_time2').prop('disabled',true);
        }
        
        // $('#specific_time1').prop('disabled',false);
        // $('#specific_time2').prop('disabled',false);

    });

    
</script>
@endpush