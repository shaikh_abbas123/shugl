@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Vendor</h3>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickForm" action="{{route('admin.vendorStore')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @if (isset($data->id))
                                <input type="hidden" name="id" value="{{ @$data->id }}">
                            @endif
                            <div class="card-body row">
                                <div class="form-group col-md-6">
                                    <label for="name">Vendor Name</label>
                                    <input type="text" name="name" class="form-control"
                                        value="{{(old('name')!=null)? (old('name')):(isset($data->name)? $data->name:'')}}" id="name">
                                    @error('name')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="email">Vendor Email</label>
                                    <input type="text" name="email" class="form-control"
                                        value="{{(old('email')!=null)? (old('email')):(isset($data->email)? $data->email:'')}}" id="email">
                                    @error('email')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" class="form-control"
                                        value="" id="password">
                                    @error('password')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="confirm_password">Confirm Password</label>
                                    <input type="password" name="confirm_password" class="form-control"
                                        value="" id="confirm_password">
                                    @error('confirm_password')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="form-check ">
                                        <input type="checkbox" class="form-check-input" name="active" id="active"  {{old('active')=="on"||@$data->active==1? 'checked':''}}>  Is Active?
                                        @error('active')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror 
                                    </div>
                                </div>


                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Save</button>
                                <a href="{{route('admin.vendor')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@push('custom-script')


<script> 
$('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                $('#quickForm').submit();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })
</script>
@endpush