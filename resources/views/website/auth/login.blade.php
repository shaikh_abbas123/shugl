<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="None">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="{{asset('backend/dist/img/icon.png')}}"/>
  <title>SHUGL - Login</title>
<head>
    

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
</head>

<body class="hold-transition login-page">
    
    <div class="login-box">
    
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
            <div class="login-banner">
                <h1>Welcome To <strong>Shugl</h1>
            </div>

                <form action="{{route('login.store')}}" method="post">
                    @if (Session::has('message'))
                    <p class="text-danger">{{Session::get('message')}}</p>
                    @endif

                    @if (Session::has('success'))
                    <p class="text-success">{{Session::get('success')}}</p>
                    @endif

                    @csrf
                    <div class="input-group mb-2">
                        <input type="email" class="form-control" value="{{old('email')}}" name="email"
                            placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    @error('email')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror
                    <div class="input-group mb-2">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    @error('password')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror
                    <div class="row sign-in-remember">
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember" name="remember">
                                <label for="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4 col-xs-12 col-sm-12">
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <div class="sign-up-login-btn">
                    Don't have an account?<a href="{{route("register")}}">Sign Up!</a>
                </div>
                <div class="forget-pass-login-btn">
                    <a href="{{route('forgotpassword','user')}}">Forgot Password?</a>
                </div>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
    

    <!-- jQuery -->
    <script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
</body>

</html>
