@extends('website.layouts.app2')

@section('content')
<section class="calender-sec">
<div class="container-fluid">
    <div class="booking-date-heading row">
        <h1 class="w-100">Select Date</h1>
    </div>
    <div class="row">
        <div mbsc-page class="demo-min-max-restrictions col col-12  col-12-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
        <div style="height:100%">
            <div id="demo-date"></div>
            
        </div>
        </div>
        <div class="col col-12 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 calender-table-col">
            <div class="dv-calender">
                <table class="calender-table">
                    <thead>
                        <th>Date</th>
                        <th>Slot</th>
                    </thead>
                    <tbody>
                        <tr>
                        <td>
                            <h3>10th</h3>
                            <h4>16:00</h4>
                        </td>
                        <td class="calender-t-right">
                            <span class="slot-dot">&nbsp;</span> 8:00am-6:00pm | 7:00pm-3:00am
                        </td>
                        </tr>
                        <tr>
                            <td>
                                <h3>10th</h3>
                                <h4>16:00</h4>
                            </td>
                            <td class="calender-t-right">
                                <span class="slot-dot">&nbsp;</span> 8:00am-6:00pm | 7:00pm-3:00am
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3>10th</h3>
                                <h4>16:00</h4>
                            </td>
                            <td class="calender-t-right"> 
                                <span class="slot-dot">&nbsp;</span> 8:00am-6:00pm | 7:00pm-3:00am
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="book-btn">
                <a href="#" class="book-new-btn">Book Now</a>
            </div>
        </div>
    </div>
</div>
</section>

<section class="popular-this-month mt-auto mb-auto pb-3 listing-page-popular">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="col title-left">
                  <h2 class="beach-hut">Beach Huts</h2>
                  <h3 class="cat-headng pop-t-m">Popular This Month</h3>
                </div>
                <div class="col back-to-home-right">
                    <a href="/" class="" style="text-decoration: none;">
                        <h3 class="back-to-home">Back to home</h3>
                    </a>
                </div>
              </div>
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>    
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>    
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>    
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>    
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>    
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>
<section class="tiles-sec booking-date-pg">
    <div class="container-fluid">
        <div class="tiles">
            <h3 class="cat-headng">Other Categories</h2>
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-beachhuts.png')}}">
                <span class="text-white">Beach Huts</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-farmhouses.png')}}">
                <span class="text-dark">Farm Houses</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-lawnsbanquets.png')}}">
                <span class="text-white">Lawns & Banquets</span>
            </a>
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-hotelsrestaurant.png')}}">
                <span class="text-dark">Hotels & Restaurants</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-catering.png')}}">
                <span class="text-white">Catering</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-transport.png')}}">
                <span class="text-dark">Transport</span>
            </a>

            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-eventdecorator.png')}}">
                <span class="text-white">Event Decorators</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow tile-med">
                <img class="cat-img" src="{{asset('frontend/images/cat-photographers.png')}}">
                <span class="text-dark">Photographers</span>
            </a>
        </div>
    </div>
</section>
@endsection
@push('custom-script')
<script>
        // Ignore this in your implementation
        window.isMbscDemo = true;
</script>

<script>

    mobiscroll.setOptions({
        locale: mobiscroll.localeEn,         // Specify language like: locale: mobiscroll.localePl or omit setting to use default
        theme: 'ios',                        // Specify theme like: theme: 'ios' or omit setting to use default
            themeVariant: 'light'            // More info about themeVariant: https://docs.mobiscroll.com/5-7-2/calendar#opt-themeVariant
    });
    
    $(function () {
        // Mobiscroll Calendar initialization
        $('#demo-date').mobiscroll().datepicker({
            controls: ['calendar'],          // More info about controls: https://docs.mobiscroll.com/5-7-2/calendar#opt-controls
            display: 'inline',               // Specify display mode like: display: 'bottom' or omit setting to use default
            min: '1920-01-01',               // More info about min: https://docs.mobiscroll.com/5-7-2/calendar#opt-min
            max: '2050-01-01'                // More info about max: https://docs.mobiscroll.com/5-7-2/calendar#opt-max
        });
    
        // Mobiscroll Calendar initialization
        $('#demo-datetime').mobiscroll().datepicker({
            controls: ['calendar', 'time'],  // More info about controls: https://docs.mobiscroll.com/5-7-2/calendar#opt-controls
            display: 'inline',               // Specify display mode like: display: 'bottom' or omit setting to use default
            min: '2000-01-01T12:00',         // More info about min: https://docs.mobiscroll.com/5-7-2/calendar#opt-min
            max: '2050-01-01T12:00'          // More info about max: https://docs.mobiscroll.com/5-7-2/calendar#opt-max
        });
    });
</script>

@endpush

@push('custom-css')
<style>
    input {
        width: 100%;
        margin: 0 0 5px 0;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 0;
        font-family: arial, verdana, sans-serif;
        font-size: 14px;
        box-sizing: border-box;
        -webkit-appearance: none;
    }

    .mbsc-page {
        padding: 1em;
    }

    /* CUSTOM NEW STYLES */
    

.mbsc-calendar-title-wrapper.mbsc-ios {
    /* background: transparent; */
    /* background: linear-gradient(22deg, rgba(214,50,74,1) 0%, rgba(234,91,85,1) 84%, rgba(234,91,85,1) 100%); */
    font-size: 28px;
    text-align: center;

}

.mbsc-calendar-title-wrapper.mbsc-ios button.mbsc-calendar-button.mbsc-reset.mbsc-font.mbsc-button.mbsc-ios.mbsc-ltr.mbsc-button-flat {
    margin: 0 auto;
    color:#fff;
}

table.calender-table th {
    font-size: 30px;
    font-weight: 500;
}

table.calender-table {
    width: 100%;
}

.mbsc-calendar-controls.mbsc-ios {
    background: rgb(214,50,74);
    background: linear-gradient(22deg, rgba(214,50,74,1) 0%, rgba(234,91,85,1) 84%, rgba(234,91,85,1) 100%);
    padding: 26px 0px;
}
.mbsc-ios.mbsc-page{
    background:transparent;
    padding:0px;
    border-radius: 10px;
    border: 1px solid #00000033;
    box-shadow: 0px 0px 5px #0000003b;
}

table.calender-table td h3 {
    font-size: 24px !important;
    font-weight: 400 !important;
}

table.calender-table td h4 {
    font-weight: 400 !important;
    font-size: 16px;
    color: #00000082;
}
.dv-calender {
    padding: 50px;
    border: 1px solid #00000033;
    border-radius: 14px;
    box-shadow: 0px 0px 5px #0000003b;
}
a.book-new-btn {
    font-size: 39px;
    background: rgb(214,50,74);
    background: linear-gradient(22deg, rgba(214,50,74,1) 0%, rgba(234,91,85,1) 84%, rgba(234,91,85,1) 100%);
    width: 100%;
    display: block;
    padding: 20px;
    text-align: center;
    color: #fff;
    font-weight: 400;
    border-radius: 15px;
    margin-top: 15px;
    text-decoration:none;
}
.mbsc-calendar-slide.mbsc-ios.mbsc-ltr {
    padding-top: 39px;
}
.mbsc-ios.mbsc-calendar-button.mbsc-button {
    color: #ffffff;
}
.mbsc-ios.mbsc-datepicker-inline{
    border-color:transparent;
}
.mbsc-ios.mbsc-selected .mbsc-calendar-cell-text {
    border-color: #e95954;
    background: #dd404e;
}

</style>
@endpush