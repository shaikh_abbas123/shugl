@extends('website.layouts.app2')
@section('content')
<div class="success-main-container">
	<div class="success-check-container">
		<div class="success-check-background">
			<svg viewBox="0 0 65 51" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M7 25L27.3077 44L58.5 7" stroke="#EC5F56" stroke-width="13" stroke-linecap="round" stroke-linejoin="round" />
			</svg>
		</div>
	</div>
    <div class="row m-0">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="text-center text-light confirm-msg">Please check your email/phone for confirmation & details</h2>
            <h4 class="text-center text-light order-num">Order # 1236459</h4>
            <div class="w-100 btn-div text-center pt-3">
                <a href="#" class="btn btn-danger text-center addmore-btn d-block">Add More</a>
                <a href="/" class="btn btn-danger text-center backtohome-btn d-block">Back To Home</a>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-css')
<style>
.confirm-msg{
    font-size: 22px;
    letter-spacing: 1px;
    font-weight: 600;
    padding: 3rem 0 1rem;
    line-height: 1.5;
}
.order-num{
    font-size: 22px;
    letter-spacing: 1px;
    font-weight: 600;
    padding: 1.5rem 0;
}
.addmore-btn{
    color: #FB1616 ;
    font-size: 20px ;
    letter-spacing: 1px;
    border-radius: 25px ;
    width: 220px;
    background-color: #FFFFFF;
    margin: 10px auto;
    padding: 10px;
    cursor: pointer;
}
.addmore-btn:hover{
    border-color: #fff;
    background-color: transparent;
    color: #000;
}
.backtohome-btn:hover{
    border-color:  #DC3545;
    background-color: #fff;
    color: #000;
}
.backtohome-btn{
    border-color: #FFFFFF ;
    color: #fff;
    margin: 10px auto;
    background-color: #DC3545;
    font-size: 20px ;
    letter-spacing: 1px;
    border-radius: 25px ;
    width: 220px;
    padding: 10px;
    cursor: pointer;
}
.success-main-container {
	width: 100%;
    height: 100vh;
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    background: linear-gradient(
22deg
, rgba(214, 50, 74, 1) 0%, rgba(234, 91, 85, 1) 84%, rgba(234, 91, 85, 1) 100%);
    /* position: absolute;
    top: 0;
    z-index: -99999999; */
}

.success-check-container {
	width: 250px;
    height: 250px;
	display: flex;
	flex-flow: column;
	align-items: center;
	justify-content: space-between;		
}
svg {
    width: 65%;
    transform: translateY(0.25rem);
    stroke-dasharray: 80;
    stroke-dashoffset: 80;
    animation: animateCheck 0.35s forwards 1.25s ease-out;
}
.success-check-shadow {
    bottom: calc(-15% - 5px);
    left: 0;
    border-radius: 50%;
    background: radial-gradient(closest-side, rgba(73, 218, 131, 1), transparent);
    animation: animateShadow 0.75s ease-out forwards 0.75s;
}
.success-check-background {
    width: 100%;
    height: calc(100% - 1.25rem);
    background: #fff;
    box-shadow: 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset,
        0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset;
    transform: scale(0.84);
    border-radius: 50%;
    animation: animateContainer 0.75s ease-out forwards 0.75s;
    display: flex;
    align-items: center;
    justify-content: center;
    opacity: 0;
}

@keyframes animateContainer {
	0% {
		opacity: 0;
		transform: scale(0);
		box-shadow: 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset,
			0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset;
	}
	25% {
		opacity: 1;
		transform: scale(0.9);
		box-shadow: 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset,
			0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset;
	}
	43.75% {
		transform: scale(1.15);
		box-shadow: 0px 0px 0px 43.334px rgba(255, 255, 255, 0.25) inset,
			0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset;
	}
	62.5% {
		transform: scale(1);
		box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset,
			0px 0px 0px 21.667px rgba(255, 255, 255, 0.25) inset;
	}
	81.25% {
		box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset,
			0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset;
	}
	100% {
		opacity: 1;
		box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset,
			0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset;
	}
}

@keyframes animateCheck {
	from {
		stroke-dashoffset: 80;
	}
	to {
		stroke-dashoffset: 0;
	}
}

@keyframes animateShadow {
	0% {
		opacity: 0;
		width: 100%;
		height: 15%;
	}
	25% {
		opacity: 0.25;
	}
	43.75% {
		width: 40%;
		height: 7%;
		opacity: 0.35;
	}
	100% {
		width: 85%;
		height: 15%;
		opacity: 0.25;
	}
}

.navbar {
    display: none;
}
</style>
@endpush