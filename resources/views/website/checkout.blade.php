@extends('website.layouts.app2')

@section('content')
<div class="checkout-pg container-fluid">
    <div class="row ">
        <div class="col col-12 col-md-12 col-lg-6 col-xl-6 col-sm-12">
            <div class="booking-date-heading row">
                <h1 class="w-100 text-center">Check Out</h1>
            </div>
            <div class="row checkout-sum">
                <div class="col col-12 col-sm-12 col-md-6">
                    <h2 class="iland-title">Charna Island</h2>
                    <p>Some text about booking of Charna Island</p>
                </div>
                <div class="col col-12 col-sm-12 col-md-6">
                    <h2 class="iland-price">Rs.30,000</h2>
                </div>
            </div>
            <div class="row checkout-cont-d">
            <div class="col col-12 col-lg-5 col-md-12 col-xs-12 col-sm-12 cont-no-col">
                <div class="cont-no-dv">
                    <p class="cont-no-heading"><strong>Contact Number</strong></p>
                    <p class="cont-bot-p">+92 31280 1992</p>
                </div>
                <div class="cont-no-dv">
                    <p class="cont-no-heading"><strong>Address</strong></p>
                    <p class="cont-bot-p">S32, left of Hawsbay Road</p>
                </div>
                <div class="cont-no-dv">
                    <p class="cont-no-heading"><strong>Date</strong></p>
                    <p class="cont-bot-p">10th August 2020</p>
                </div>
            </div>
            <div class="col col-12 col-lg-7 col-md-12 col-xs-12 col-sm-12 checkout-total">
                <div class="sub-total">
                    <table class="checkout-total-table">
                        <tr>
                            <td>Subtotal</td>
                            <td class="sub-total">Rs.30,000</td>
                        </tr>
                        <tr>
                            <td>GST</td>
                            <td class="gst">Rs.1,000</td>
                        </tr>
                    </table>
                    <div class="voucher-link-dv">
                        <a href="#" class="voucher-link">Do you have a voucher?</a>
                    </div>
                </div>
                <div>    
                   <h3 class="pay-heading">Payment methods</h3>
                </div>
                <div class="row payment-btns">
                    
                    <div class="col">
                        <a href="#" class="cod-btn">Cash on delivery</a>
                    </div>
                    <div class="col online-btn-div">
                        <a href="#" class="online-btn">Online Payment</a>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="confirm-booking row">
            <a href="#" class="book-btn-cnfrm-link"> <span class="cnfrm-bk-title">Confirm Booking</span><span class="cnfrm-booking-price">RS.31,000</span></a>
        </div>
            
        </div>
        <div class="col col-12 col-md-12 col-lg-6 col-xl-6 col-sm-12">
            <section class="recently-added mt-auto mb-auto pb-3 pt-3 listing-page-recently checkout-addon">
                
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3 class="cat-headng rec-added">Popular with this package</h3>
                            <div class="row">
                                <div class="col col-12 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <a href="#" class="custm-card">
                                        <div class="card">
                                            <div class="heart static">
                                                <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                            </div>
                                            <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                            <div class="card-body">
                                                <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col col-12 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <a href="#" class="custm-card">
                                        <div class="card">
                                            <div class="heart static">
                                                <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                            </div>
                                            <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                                            <div class="card-body">
                                                <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col col-12 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <a href="#" class="custm-card">
                                        <div class="card">
                                            <div class="heart static">
                                                <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                            </div>
                                            <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                            <div class="card-body">
                                                <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                                
                            </div>
                        </div>
                        
                    </div>
                
            </section>
            <section class="tiles-sec checkout-tiles">
                    <div class="tiles">
                        <h3 class="cat-headng">Other Categories</h2>
                        <a href="#" class="tile icon-tile shadow clr-red">
                            <img class="cat-img" src="{{asset('frontend/images/cat-beachhuts.png')}}">
                            <span class="text-white">Beach Huts</span>
                        </a>
                        
                        <a href="#" class="tile icon-tile shadow clr-white">
                            <img class="cat-img" src="{{asset('frontend/images/cat-farmhouses.png')}}">
                            <span class="text-dark">Farm Houses</span>
                        </a>
                        
                        <a href="#" class="tile icon-tile shadow clr-red">
                            <img class="cat-img" src="{{asset('frontend/images/cat-lawnsbanquets.png')}}">
                            <span class="text-white">Lawns & Banquets</span>
                        </a>
                        <a href="#" class="tile icon-tile shadow clr-white">
                            <img class="cat-img" src="{{asset('frontend/images/cat-hotelsrestaurant.png')}}">
                            <span class="text-dark">Hotels & Restaurants</span>
                        </a>
                        
                        <a href="#" class="tile icon-tile shadow clr-red">
                            <img class="cat-img" src="{{asset('frontend/images/cat-catering.png')}}">
                            <span class="text-white">Catering</span>
                        </a>
                        
                        <a href="#" class="tile icon-tile shadow clr-white">
                            <img class="cat-img" src="{{asset('frontend/images/cat-transport.png')}}">
                            <span class="text-dark">Transport</span>
                        </a>

                        <a href="#" class="tile icon-tile shadow clr-red">
                            <img class="cat-img" src="{{asset('frontend/images/cat-eventdecorator.png')}}">
                            <span class="text-white">Event Decorators</span>
                        </a>
                        
                        <a href="#" class="tile icon-tile shadow tile-med">
                            <img class="cat-img" src="{{asset('frontend/images/cat-photographers.png')}}">
                            <span class="text-dark">Photographers</span>
                        </a>
                    </div>
            </section>
        </div>
    </div>
</div>
@endsection
