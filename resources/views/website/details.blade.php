@extends('website.layouts.app2')

@section('content')
<section class="slider">
    <div class="container-fluid pt-0 pr-pl-15 ">
        <div class="carousel-container position-relative">
          
        <!-- Sorry! Lightbox doesn't work - yet. -->
          
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active" data-slide-number="0">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/Pn6iimgM-wo/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="1">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/tXqVe7oO-go/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="2">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/qlYQb7B9vog/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="3">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/QfEfkWk1Uhk/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="4">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/CSIcgaLiFO0/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="5">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/a_xa7RUKzdc/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="6">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/uanoYn1AmPs/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="7">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/_snqARKTgoc/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="8">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/M9F8VR0jEPM/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
            <div class="carousel-item" data-slide-number="9">
              <img src="{{asset('frontend/images/slider-img.png')}}" class="d-block w-100 h-65" alt="..." data-remote="https://source.unsplash.com/Q1p7bh3SHj8/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
            </div>
          </div>
        </div>
        
        <!-- Carousel Navigation -->
        <div id="carousel-thumbs" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row mx-0 sliders-row">
                <div id="carousel-selector-0" class=" col-4 col-sm-2 mt-auto mb-auto p-2 selected" data-target="#myCarousel" data-slide-to="0">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-1" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="1">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-2" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="2">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-3" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="3">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-4" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="4">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-5" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="5">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row mx-0 sliders-row">
                <div id="carousel-selector-5" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="5">
                    <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-6" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="6">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-7" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="7">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-8" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="8">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-9" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="9">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
                <div id="carousel-selector-10" class=" col-4 col-sm-2 mt-auto mb-auto p-2" data-target="#myCarousel" data-slide-to="10">
                  <img src="{{asset('frontend/images/slider-img.png')}}" class="slider-img" alt="...">
                </div>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carousel-thumbs" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carousel-thumbs" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        
        </div> <!-- /row -->
        </div> <!-- /container -->
</section>
<section class="">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-12 mt-4">
                <div class="row mb-2">
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 col col-12 card col-spacing shadow custm-wdth1">
                        <div class="row">
                            <div class="col-lg-9 col-9 col-md-8 col">
                                <h5 class="hut-name mb-0">Charna island</h5>
                                <span class="small-headng">Beach</span>
                                <div class="timing-div">
                                    <img src="{{asset('frontend/images/time.png')}}" class="mr-1 ml-1" width="10px" height="10px" alt="">
                                    <span class="small-headng">Visiting time morning</span>
                                </div>
                                <div class="location-div">
                                    <img src="{{asset('frontend/images/location.png')}}" class="mr-1 ml-1" width="10px" height="10px" alt="">
                                    <span class="small-headng">S-32,Left of Hawksby Road,</span>
                                </div>
                                <div class="direction-div">
                                    <img src="{{asset('frontend/images/direction.png')}}" class="mr-1 ml-1" width="10px" height="10px" alt="">
                                    <span class="small-headng"><a href="">Directions</a>&nbsp;&nbsp;(3 miles)</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-3 col-md-4 col text-right resp-ratng-checkin ">
                                <span class="badge badge-rating">4.5</span>
                                <h3 class="text-left pt-2 checkin-text">98 Check Ins</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col col-12  card col-spacing shadow mr-auto ml-auto bg-clr custm-wdth2">
                        <div class="mt-auto mb-auto">
                            <p class="rate-place d-inline-block">Rate Place</p>
                            <fieldset class="rating rate-place d-inline-block float-right">
                                <input type="radio" id="star5" name="rating" value="5"><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="rating" value="4 and a half"><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="rating" value="4"><label class="full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="rating" value="3 and a half"><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="rating" value="3"><label class="full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="rating" value="2 and a half"><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="rating" value="2"><label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="rating" value="1 and a half"><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="rating" value="1"><label class="full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="rating" value="half"><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                            </fieldset>
                        </div>
                    </div> 
                </div>
                <div class="row mb-2">
                    <div class="col-12 p-0">
                        <h5 class="det-hut hut-name">About Hut</h5>
                    </div>
                    <div class="col-12 card col-spacing shadow mr-auto ml-auto ">
                        <p class="m-auto pt-1 pb-4 hut-detail-text"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum est ultricies integer quis. Iaculis urna id volutpat lacus laoreet. Mauris vitae ultricies leo integer malesuada. Ac odio tempor orci dapibus ultrices in. Egestas diam in arcu cursus euismod. Dictum fusce ut placerat orci nulla. Tincidunt ornare massa eget egestas purus viverra accumsan in nisl. Tempor id eu nisl nunc mi ipsum faucibus. Fusce id velit ut tortor pretium. Massa ultricies mi quis hendrerit dolor magna eget…. <a href="#">read more</a></p>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-12 p-0">
                        <h5 class="det-hut hut-name">Facilities</h5>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-12 col card col-spacing shadow mr-auto ml-auto custm-wdth3">
                      <div class="row">  
                        <div class="col col-6 col-lg-3 col-md-6 col-sm-6 col-xs-12 facilities-col">
                          <ul class="facilities-list p-0">
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Electricity</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Kitchen</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Dining Table(s)</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Water</span>
                            </li>
                          </ul>  
                        </div>
                        <div class="col col-6 col-lg-3 col-md-6 col-sm-6 col-xs-12 facilities-col">
                          <ul class="facilities-list p-0">
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Generator</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Microwave</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Chair(s)</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Gas</span>
                            </li>
                          </ul>  
                        </div>
                        <div class="col col-6 col-lg-3 col-md-6 col-sm-6 col-xs-12 facilities-col">
                          <ul class="facilities-list p-0">
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Parking</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Swimming Pool</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Bed(s)</span>
                            </li>
                          </ul>  
                        </div>
                        <div class="col col-6 col-lg-3 col-md-6 col-sm-6 col-xs-12 facilities-col">
                          <ul class="facilities-list p-0">
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Air Conditioning</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Sofa(s)</span>
                            </li>
                            <li>
                              <input type="checkbox" checked="checked">
                              <span class="checkmark">Outdoor Furniture</span>
                            </li>
                          </ul> 
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12 col card col-spacing shadow mr-auto ml-auto bg-clr custm-wdth4">
                      <a href="#" class="check-availability-btn m-auto text-center">
                        <img class="availability-img" src="{{asset('frontend/images/availability-img.png')}}" alt="">
                        <h4 class="m-auto pt-2 pb-2 availability-headng">Check Availability</h4>
                      </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 col-12 col-spacing bg-none">
                <img class="ad-banner2-listing" src="{{asset('frontend/images/img1.png')}}" alt="">
            </div>
        </div>   
    </div>
</section>
<section class="popular-this-month mt-auto mb-auto pb-3 listing-page-popular">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="col title-left">
                  <h2 class="beach-hut">Beach Huts</h2>
                  <h3 class="cat-headng pop-t-m">Popular This Month</h3>
                </div>
              </div>
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                      <a href="#" class="custm-card">  
                        <div class="card">
                            <div class="heart static">
                                <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                            </div>
                            <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                            <div class="card-body">
                                <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                            </div>
                        </div>
                      </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                      <a href="#" class="custm-card">  
                        <div class="card">
                            <div class="heart static">
                                <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                            </div>
                            <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                            <div class="card-body">
                                <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                            </div>
                        </div>
                      </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                      <a href="#" class="custm-card">  
                        <div class="card">
                            <div class="heart static">
                                <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                            </div>
                            <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                            <div class="card-body">
                                <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                            </div>
                        </div>
                      </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                      <a href="#" class="custm-card">  
                        <div class="card">
                            <div class="heart static">
                                <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                            </div>
                            <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                            <div class="card-body">
                                <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                            </div>
                        </div>
                      </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                      <a href="#" class="custm-card">  
                        <div class="card">
                            <div class="heart static">
                                <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                            </div>
                            <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                            <div class="card-body">
                                <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                            </div>
                        </div>
                      </a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>
<section class="tiles-sec">
    <div class="container-fluid">
        <div class="tiles">
            <h3 class="cat-headng">Other Categories</h2>
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-beachhuts.png')}}">
                <span class="text-white">Beach Huts</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-farmhouses.png')}}">
                <span class="text-dark">Farm Houses</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-lawnsbanquets.png')}}">
                <span class="text-white">Lawns & Banquets</span>
            </a>
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-hotelsrestaurant.png')}}">
                <span class="text-dark">Hotels & Restaurants</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-catering.png')}}">
                <span class="text-white">Catering</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-transport.png')}}">
                <span class="text-dark">Transport</span>
            </a>

            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-eventdecorator.png')}}">
                <span class="text-white">Event Decorators</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow tile-med">
                <img class="cat-img" src="{{asset('frontend/images/cat-photographers.png')}}">
                <span class="text-dark">Photographers</span>
            </a>
        </div>
    </div>
</section>
@endsection