@extends('website.layouts.app2')

@section('content')
<section class="booking-forms">
    <div class="container beachhut">
        <div class="booking-date-heading">
            <h1>Farm House Details</h1>
        </div>
        <form action="#">
            <div class="row form-first-row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Venue Name">
                </div>
                
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Address">
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <input type="text" placeholder="City">
                </div>
            </div>
            <div class="row sec-row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <textarea placeholder="Description"></textarea>
                </div>
                
            </div>
            <div class="row form-sec-row">
                <div class="col-md-6 col-sm-12 col-xs-12 col-left">
                    <div class=" col form-lable-dv">
                        <label>Service Hours</label>
                    </div>
                    <div class="col">
                        <ul class="">
                            <li>
                                <input type="checkbox" checked="checked">
                                <span class="checkmark">24 hours</span>
                            </li>
                            <li>
                                <input type="checkbox" checked="checked">
                                <span class="checkmark">12 Hours</span>
                            </li>
                            <li>
                                <input type="checkbox" checked="checked">
                                <span class="checkmark">Specific</span>
                            </li>
                        </ul> 
                    </div>
                    <div class=" col timeselection">
                        
                            <select>
                                <option>8:am</option>
                                <option>9:am</option>
                            </select>
                        
                        
                            <select>
                                <option>12:am</option>
                                <option>3:am</option>
                            </select>
                        
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 col-right">
                    <div class="col form-lable-dv">
                        <label>Minimum Slot(Choose One)</label>
                    </div>
                <div class="col">
                    <ul class="">
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">4 hours</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">6 Hours</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">8 Hours</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Others</span>
                        </li>
                    </ul> 
                    </div>
                    <div class="col timeselection">
                        
                        <select>
                            <option>1 Hour</option>
                            <option>2 Hour</option>
                            <option>3 Hour</option>
                            <option>4 Hour</option>
                        </select>
                        
                    </div>
                </div> 
            </div>
            <div class="row form-third-row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Area (Sq Ft)">
                </div>
                
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Number of Floors">
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Number of Rooms">
                </div>
            </div>
            <div class="row form-four-row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Rent per slot">
                </div>
                
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Rent 24 hours">
                </div>
                <!-- <div class="col">
                    <input type="text" placeholder="Facilities">
                </div> -->
            </div>
            <div class="form-lable-dv">
                <label>Facilities</label>
            </div>
            <div class="row form-facilities">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <ul class="">
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Electricity</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Kitchen</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Dining Table</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Water</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <ul class="">
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Generator</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Microwave</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Chair(s)</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Gas</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <ul class="">
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Parking</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Swimming Pool</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Bed(s)</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <ul class="">
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Air Conditioning</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Sofa(s)</span>
                        </li>
                        <li>
                            <input type="checkbox" checked="checked">
                            <span class="checkmark">Outdoor Furniture</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row pic-upload">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <label class="uploadlabel"> Upload Pictures
                        <input type="file" id="myfile" name="myfile">
                    </label>
                </div>
            </div>
            <div class="row subit-row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="submit" value="Next">
                </div>
            </div>
        </form>
    </div>
        
    
</section>

@endsection
@push('custom-script')


@endpush

@push('custom-css')

@endpush