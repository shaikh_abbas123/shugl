@extends('website.layouts.app2')

@section('content')
<section class="booking-forms">
    <div class="container beachhut">
        <div class="booking-date-heading">
            <h1>Hotel Details</h1>
        </div>
        <form action="#">
            <div class="row form-first-row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Hotel Name">
                </div>
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Address">
                </div>
            </div>
            <div class="row sec-row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <textarea placeholder="Description"></textarea>
                </div>
                
            </div>
               
            @include("website.forms.minimumGuestComponent") 
                
            
            <div class="row subit-row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input class="next-btn" type="submit" value="Next">
                </div>
            </div>
        </form>
    </div>
        
    
</section>

@endsection
@push('custom-script')


@endpush

@push('custom-css')

@endpush