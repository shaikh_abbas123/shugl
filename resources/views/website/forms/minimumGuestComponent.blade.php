<div class="row service-decor-custom">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-left">
        <div class=" col form-lable-dv pt-2">
            <label>Service Hours</label>
        </div>
        <div class="col">
            <ul class="">
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">24 hours</span>
                </li>
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">12 Hours</span>
                </li>
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">Specific</span>
                </li>
            </ul> 
        </div>
        <div class=" col timeselection">
            
                <select>
                    <option>8:am</option>
                    <option>9:am</option>
                </select>
            
            
                <select>
                    <option>12:am</option>
                    <option>3:am</option>
                </select>
                <input type="text" placeholder="Seating Capacity">
        </div>
        
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-right">
        <div class="col form-lable-dv pt-2">
            <label>Decor Packages</label>
        </div>
        <div class="col">
            <div class="row">
            <div class="col">
                <ul class="">
                    <li>
                        <input type="checkbox" checked="">
                        <span class="checkmark">Platinum</span>
                        
                    </li>
                    <li>
                        <input type="checkbox" checked="">
                        <span class="checkmark">Gold</span>
                        
                    </li>
                    <li>
                        <input type="checkbox" checked="">
                        <span class="checkmark">Silver</span>
                        
                        
                    </li>
                </ul> 
            </div>
            <div class="col">
            <ul class="add-details-ul">
                <li> 
                    <a href="#" class="form-add-details-btn">Add details</a>
                </li>
                <li>
                    <a href="#" class="form-add-details-btn">Add details</a>
                </li>
                <li>
                    <a href="#" class="form-add-details-btn">Add details</a>
                </li>
            </ul>
            </div>
            </div>
            
        </div>
       
    </div> 
</div>

<div class="row form-sec-row mt-2">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-left">
        <div class=" col form-lable-dv">
            <label>Minimum Guest</label>
        </div>
        <div class="col">
            <ul class="">
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">25 Persons</span>
                </li>
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">50 Persons</span>
                </li>
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">100 Persons</span>
                </li>
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">Other</span>
                </li>
            </ul>
            
        </div>
        <div class=" col timeselection">
            <select>
                <option>150 Persons</option>
                <option>200 Persons</option>
                <option>300 Persons</option>
                <option>400 Persons</option>
            </select>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-right">
        <div class="col form-lable-dv">
            <label>Minimum Slot(Choose One)</label>
        </div>
        <div class="col">
            <ul class="">
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">4 hours</span>
                </li>
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">6 Hours</span>
                </li>
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">8 Hours</span>
                </li>
                <li>
                    <input type="checkbox" checked="checked">
                    <span class="checkmark">Others</span>
                </li>
            </ul> 
        </div>
        <div class="col timeselection">
            
            <select>
                <option>1 Hour</option>
                <option>2 Hour</option>
                <option>3 Hour</option>
                <option>4 Hour</option>
                <option>5 Hour</option>
            </select>
        </div>
    </div> 
</div>
<div class="row pic-upload">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label class="uploadlabel"> Upload Pictures
            <input class="next-btn" type="file" id="myfile" name="myfile">
        </label>
    </div>
</div>