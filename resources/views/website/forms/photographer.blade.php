@extends('website.layouts.app2')
@section('content')
<section class="booking-forms">
    <div class="container beachhut">
        <div class="booking-date-heading">
            <h1>Photographer Details</h1>
        </div>
        <form action="#">
            <div class="row form-first-row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Photographer Name">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Address">
                </div>
            </div>
            <div class="row sec-row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <textarea placeholder="Description"></textarea>
                </div>
            </div>
            <div class="row sec-row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="text" placeholder="Equipments">
                </div>
            </div>
            <div class="row service-decor-custom">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-left">
                    <div class=" col form-lable-dv">
                        <label>Service Hours</label>
                    </div>
                    <div class="col">
                        <ul class="">
                            <li>
                                <input type="checkbox" checked="checked">
                                <span class="checkmark">24 hours</span>
                            </li>
                            <li>
                                <input type="checkbox" checked="checked">
                                <span class="checkmark">12 Hours</span>
                            </li>
                            <li>
                                <input type="checkbox" checked="checked">
                                <span class="checkmark">Specific</span>
                            </li>
                        </ul> 
                    </div>
                    <div class=" col timeselection">
                            <select>
                                <option>8:am</option>
                                <option>9:am</option>
                            </select>
                            <select>
                                <option>12:am</option>
                                <option>3:am</option>
                            </select>
                            <input type="text" placeholder="Seating Capacity">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-right">
                    <div class="col form-lable-dv">
                        <label>Specialities</label>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <ul class="">
                                    <li>
                                        <input type="checkbox" checked="">
                                        <span class="checkmark">Events</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="">
                                        <span class="checkmark">Wedding</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="">
                                        <span class="checkmark">Drone</span> 
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="">
                                        <span class="checkmark">Concert</span> 
                                    </li>
                                </ul>
                            </div>
                            <div class="col">
                                <ul class="">
                                    <li>
                                        <input type="checkbox" checked="">
                                        <span class="checkmark">Events</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="">
                                        <span class="checkmark">Wedding</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="">
                                        <span class="checkmark">Drone</span> 
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="">
                                        <span class="checkmark">Concert</span> 
                                    </li>
                                </ul>
                            </div>
                            <input type="text" placeholder="Other Specialities">
                        </div> 
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-lable-dv">
                            <label>Minimum Slot(Choose One)</label>
                        </div>
                            <ul class="">
                                <li>
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark">4 hours</span>
                                </li>
                                <li>
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark">6 Hours</span>
                                </li>
                                <li>
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark">8 Hours</span>
                                </li>
                                <li>
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark">Others</span>
                                </li>
                            </ul> 
                        <div class="timeselection">
                            <select>
                                <option>1 Hour</option>
                                <option>2 Hour</option>
                                <option>3 Hour</option>
                                <option>4 Hour</option>
                                <option>5 Hour</option>
                            </select>
                            <input type="text" placeholder="Charges Per Slot">
                            <input type="text" placeholder="Other Services">
                        </div>
                </div> 
            </div>
            <div class="row pic-upload mt-2">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label class="uploadlabel"> Upload Pictures
                        <input type="file" id="myfile" name="myfile">
                    </label>
                </div>
            </div>    
            <div class="row subit-row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input class="next-btn" type="submit" value="Next">
                </div>
            </div>
        </form>
    </div>
</section>
@endsection
@push('custom-script')
@endpush
@push('custom-css')
@endpush