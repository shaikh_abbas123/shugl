@extends('website.layouts.app')

@section('content')

	<div class="hero-container">
        <div class="hero-text">
		    <h1 class="hero-title">Over 3000+ Venues and Services at your finger tips</h1>
        </div>
        <div class="hero-btn">
		    <button class="btn btn-danger text-center explore-btn">{{ __('lang.explore') }}</button>
        </div>
	</div>
</section>
<section class="tiles-sec">
    <div class="container-fluid">
        <div class="tiles">
            <h3 class="cat-headng">Categories</h2>
            <?php
                $class = 'white';
                $class_text = 'dark';
            ?>
            @foreach ( @$category as $key => $item)
            <?php
                if($class =='red')
                {
                    $class = 'white';
                    $class_text = 'dark';
                }else if($class == 'white')
                {
                    $class = 'red';
                    $class_text = 'white';
                }
            ?>
            <a href="#" class="tile icon-tile shadow clr-<?= $class?>">
                <img class="cat-img" src="{{ @asset('backend/images/category')."/".@$item->image}}">
                <span class="text-<?= $class_text?>">{{ $item->translate()->name }}</span>
            </a>
            @endforeach
            
                
        </div>
    </div>
</section>
<section class="popular-this-month mt-auto mb-auto pb-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <h3 class="cat-headng">Popular This Month</h3>
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-3 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card-resp-1024 card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-3 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card-resp-1024 card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-3 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card-resp-1024 card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-3 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card-resp-1024 card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
              <img class="ad-banner1" src="{{asset('frontend/images/img1.png')}}" alt="">
            </div>
        </div>
    </div>
</section>
<section class="recently-added mt-auto mb-auto pb-5 pt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <h3 class="cat-headng">Recently Added</h3>
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-3 custom-width">
                        <a href="#" class="custm-card">    
                            <div class="card-resp-1024 card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-3 custom-width">
                        <a href="#" class="custm-card">    
                            <div class="card-resp-1024 card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-3 custom-width">
                        <a href="#" class="custm-card">    
                            <div class="card-resp-1024 card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-3 custom-width">
                        <a href="#" class="custm-card">    
                            <div class="card-resp-1024 card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                <img class="ad-banner2" src="{{asset('frontend/images/img1.png')}}" alt="">
            </div>
        </div>
    </div>
</section>
@endsection
