<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="None">
  <meta name="author" content="">
  {{-- <link rel="icon" type="image/png" href="{{asset('backend/dist/img/icon.png')}}"/> --}}
  <title>SHUGL</title>
  @include('website.partials.style')
</head>
<!-- Header -->
<body>
  <header role="banner">
  <nav class="navbar navbar-expand-lg navbar-dark" style="background: linear-gradient(22deg, rgba(214, 50, 74, 1) 0%, rgba(234, 91, 85, 1) 84%, rgba(234, 91, 85, 1) 100%);">
      <a class="navbar-brand d-lg-none" href="#">
        SHUGL
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbarToggler7"
          aria-controls="myNavbarToggler7" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="myNavbarToggler7">
          <ul class="navbar-nav mx-auto custom-nav-width">
            <li class="nav-item">
              <a class="d-none d-lg-block" href="{{url('')}}">SHUGL</a>
            </li>
            <div class="d-none d-lg-block w-500 search-div" href="{{url('')}}">
              <form action="">
                <div class="p-1 search-input-grp rounded rounded-pill">
                  <div class="input-group">
                  <input type="search" placeholder="What are you looking for?" aria-describedby="button-addon1" class="form-control border-0 bg-light search-input">
                  <div class="input-group-append">
                    <button id="button-addon1" type="submit" class="btn btn-link text-primary"><i class="fa fa-search"></i></button>
                  </div>
                  </div>
                </div>
              </form> 
            </div>
            <li class="nav-item">
              <a class="nav-link"  href="{{url('')}}">Sign In</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('')}}">Become A Vendor</a>
            </li>
          </ul>
      </div>
  </nav>
  </header><!-- header role="banner" -->
@yield('content')
<!-- Footer -->

</body>

@include('website.partials.script')