@extends('website.layouts.app2')

@section('content')


<section class="popular-this-month mt-auto mb-auto pb-3 listing-page-popular">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="col title-left">
                  <h2 class="beach-hut">Beach Huts</h2>
                  <h3 class="cat-headng pop-t-m">Popular This Month</h3>
                </div>
                <div class="col back-to-home-right">
                    <a href="/" class="" style="text-decoration: none;">
                        <h3 class="back-to-home">Back to home</h3>
                    </a>
                </div>
              </div>
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>
<section class="recently-added mt-auto mb-auto pb-3 pt-3 listing-page-recently">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 class="cat-headng rec-added">Recently Added</h3>
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-2 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>
<section class="popular-this-month mt-auto mb-auto pb-3">
    <div class="container-fluid">
        <div class="row pt-4 pb-4">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <h3 class="cat-headng all-beach-huts">All Beach Huts</h3>
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-4 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-4 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-4 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                <a href="#" class="custm-card">
                    <img class="ad-banner1-listing" src="{{asset('frontend/images/img1.png')}}" alt="">
                </a>    
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-4 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img1.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Charna Island</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-4 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img2.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6 col-lg-4 custom-width">
                        <a href="#" class="custm-card">
                            <div class="card">
                                <div class="heart static">
                                    <svg id="heart" width="40px" height="35px"><path id="heart-path" class="love" d="M 20 9 V 9 C 24 0 32 0 36 8.64 S 20 30 20 30.24 C 20 30.24 20 30 20 30.24 S 0 17.28 4 8.64 C 8 0 16 0 20 9"></path></svg>
                                </div>
                                <img class="card-img-top" src="{{asset('frontend/images/img3.png')}}" alt="">
                                <div class="card-body">
                                    <h4 class="card-title mi-card-title pb-3">Turtle Beach</h4>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted">7 Rooms - 30 Guests</h6>
                                    <h6 class="card-subtitle mi-card-subtitle mb-2 text-muted pt-2">13 - 18 Nov</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                <a href="#" class="custm-card">
                    <img class="ad-banner2-listing" src="{{asset('frontend/images/img1.png')}}" alt="">
                </a>    
            </div>
        </div>
    </div>
</section>

<section class="tiles-sec">
    <div class="container-fluid">
        <div class="tiles mt-0">
            <h3 class="cat-headng">Other Categories</h3>
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-beachhuts.png')}}">
                <span class="text-white">Beach Huts</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-farmhouses.png')}}">
                <span class="text-dark">Farm Houses</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-lawnsbanquets.png')}}">
                <span class="text-white">Lawns & Banquets</span>
            </a>
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-hotelsrestaurant.png')}}">
                <span class="text-dark">Hotels & Restaurants</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-catering.png')}}">
                <span class="text-white">Catering</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow clr-white">
                <img class="cat-img" src="{{asset('frontend/images/cat-transport.png')}}">
                <span class="text-dark">Transport</span>
            </a>

            <a href="#" class="tile icon-tile shadow clr-red">
                <img class="cat-img" src="{{asset('frontend/images/cat-eventdecorator.png')}}">
                <span class="text-white">Event Decorators</span>
            </a>
            
            <a href="#" class="tile icon-tile shadow tile-med">
                <img class="cat-img" src="{{asset('frontend/images/cat-photographers.png')}}">
                <span class="text-dark">Photographers</span>
            </a>
        </div>
    </div>
</section>
@endsection
