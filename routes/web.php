<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//localization
Route::group(['prefix' => 'lang'], function () {
    Route::get('/{lang}', 'website\LocalizationController@index')->name('lang');
});

Route::get('/',"website\HomeController@index")->name('home');
Route::get('/listing',"website\HomeController@listing")->name('listing');
Route::get('/details',"website\HomeController@details")->name('details');
Route::get('/bookingdate',"website\HomeController@bookingdate")->name('bookingdate');
Route::get('/bookingsuccess',"website\HomeController@bookingsuccess")->name('bookingsuccess');
Route::get('/checkout',"website\HomeController@checkout")->name('checkout');
Route::get('/forms/beachhutdetails',"website\HomeController@beachhutdetails")->name('beachhutdetails');
Route::get('/forms/farmhousedetails',"website\HomeController@farmhousedetails")->name('farmhousedetails');
Route::get('/forms/banquetdetails',"website\HomeController@banquetdetails")->name('banquetdetails');
Route::get('/forms/hoteldetails',"website\HomeController@hoteldetails")->name('hoteldetails');
Route::get('/forms/transporterdetails',"website\HomeController@transporterdetails")->name('transporterdetails');
Route::get('/forms/cateringdetails',"website\HomeController@cateringdetails")->name('cateringdetails');
Route::get('/forms/photographer',"website\HomeController@photographer")->name('photographer');


// VENDOR FRONTEND DASHBOARD
Route::get('/vendor/dashboard',"website\HomeController@vendordashboard")->name('vendordashboard');


Route::get('/login',"website\AuthController@loginShow")->name('login')->middleware('guest:web');
Route::post('/login',"website\AuthController@loginPost")->name('login.store');
Route::get('/logout',"website\AuthController@logout")->name('logout')->middleware('auth:web');

Route::middleware('auth:web')->group(function () {
    Route::get('home',"website\HomeController@index")->name('home');
});


Route::get('/admin/login',"Admin\AuthController@loginShow")->name('admin.login')->middleware('guest:admin');
Route::post('/admin/login',"Admin\AuthController@loginPost")->name('admin.login.store');
Route::get('/admin/logout',"Admin\AuthController@logout")->name('admin.logout')->middleware('auth:admin');


Route::get('/forgotpassword/{type}',            "ForgetPasswordResetController@viewForgotpassword")                 ->name('forgotpassword');
Route::get('/resetPassword/{email_encode}',     "ForgetPasswordResetController@ResetPassword")                      ->name('resetPassword'); // Forgot Password send email 
Route::post('/SetNewPassword',                  "ForgetPasswordResetController@SetNewPassword")                     ->name('SetNewPassword'); // Set New Password 
Route::post('/AjaxCallForForgotPasswordEmail',  "ForgetPasswordResetController@AjaxCallForForgotPasswordEmail")     ->name('AjaxCallForForgotPasswordEmail'); // Forgot Password send email 

Route::get('register',"website\RegisterController@viewRegister")->name("register");
Route::post('register',"website\RegisterController@registerUser")->name("registerUser");



Route::middleware('auth:admin')->group(function () {
    Route::get('admin/home',"Admin\HomeController@index")->name('admin.home');


    Route::get( 'admin/vendor',             "Admin\VendorController@index")     ->name('admin.vendor');
    Route::get( 'admin/vendor/create',      "Admin\VendorController@create")    ->name('admin.vendorCreate');
    Route::post('admin/vendor/store',       "Admin\VendorController@store")     ->name('admin.vendorStore');
    Route::get( 'admin/vendor/edit/{id}',   "Admin\VendorController@edit")      ->name('admin.vendorEdit');
    Route::get( 'admin/vendor/delete/{id}', "Admin\VendorController@destroy")   ->name('admin.vendorDelete');


    Route::get('admin/categories',              "Admin\CategoryController@index")   ->name('admin.category');
    Route::get('admin/categories/create',       "Admin\CategoryController@create")  ->name('admin.categoryCreate');
    Route::post('admin/categories/store',       "Admin\CategoryController@store")   ->name('admin.categoryStore');
    Route::get('admin/categories/edit/{id}',    "Admin\CategoryController@edit")    ->name('admin.categoryEdit');


    Route::get( 'admin/addOn',             "Admin\AddOnController@index")     ->name('admin.addOn');
    Route::get( 'admin/addOn/create',      "Admin\AddOnController@create")    ->name('admin.addOnCreate');
    Route::post('admin/addOn/store',       "Admin\AddOnController@store")     ->name('admin.addOnStore');
    Route::get( 'admin/addOn/edit/{id}',   "Admin\AddOnController@edit")      ->name('admin.addOnEdit');
    Route::get( 'admin/addOn/delete/{id}', "Admin\AddOnController@destroy")   ->name('admin.addOnDelete');


    Route::get( 'admin/product',             "Admin\ProductController@index")     ->name('admin.product');
    Route::get( 'admin/product/create',      "Admin\ProductController@create")    ->name('admin.productCreate');
    Route::post('admin/product/store',       "Admin\ProductController@store")     ->name('admin.productStore');
    Route::get( 'admin/product/edit/{id}',   "Admin\ProductController@edit")      ->name('admin.productEdit');
    Route::get( 'admin/product/delete/{id}', "Admin\ProductController@destroy")   ->name('admin.productDelete');


    Route::get( 'admin/facility',             "Admin\FacilityController@index")     ->name('admin.facility');
    Route::get( 'admin/facility/create',      "Admin\FacilityController@create")    ->name('admin.facilityCreate');
    Route::post('admin/facility/store',       "Admin\FacilityController@store")     ->name('admin.facilityStore');
    Route::get( 'admin/facility/edit/{id}',   "Admin\FacilityController@edit")      ->name('admin.facilityEdit');
    Route::get( 'admin/facility/delete/{id}', "Admin\FacilityController@destroy")   ->name('admin.facilityDelete');

    Route::get( 'admin/speciality',             "Admin\SpecialityController@index")     ->name('admin.speciality');
    Route::get( 'admin/speciality/create',      "Admin\SpecialityController@create")    ->name('admin.specialityCreate');
    Route::post('admin/speciality/store',       "Admin\SpecialityController@store")     ->name('admin.specialityStore');
    Route::get( 'admin/speciality/edit/{id}',   "Admin\SpecialityController@edit")      ->name('admin.specialityEdit');
    Route::get( 'admin/speciality/delete/{id}', "Admin\SpecialityController@destroy")   ->name('admin.specialityDelete');

    Route::get( 'admin/faq',             "Admin\FaqController@index")     ->name('admin.faq');
    Route::get( 'admin/faq/create',      "Admin\FaqController@create")    ->name('admin.faqCreate');
    Route::post('admin/faq/store',       "Admin\FaqController@store")     ->name('admin.faqStore');
    Route::get( 'admin/faq/edit/{id}',   "Admin\FaqController@edit")      ->name('admin.faqEdit');
    Route::get( 'admin/faq/delete/{id}', "Admin\FaqController@destroy")   ->name('admin.faqDelete');

});

Route::middleware(['auth:admin', 'auth:vendor'])->group(function () {

});


